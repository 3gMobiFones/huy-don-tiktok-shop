# huy-don-tiktok-shop
2 Cách hủy đơn hàng đã mua trên TikTok Shop hiệu quả 100%
<p style="text-align: justify;"><a href="https://blogvn.org/huy-don-hang-tiktok-shop.html"><strong>Cách hủy đơn hàng trên TikTok shop</strong></a> vô cùng đơn giản và dễ thực hiện nếu bạn có ý định thay đổi trong chớp mắt. Cũng giống như Shopee, Lazada thì TikTok Shop cũng có tính năng hủy hàng cho người dùng khi chưa vận chuyển và đã vận chuyển. Trường hợp bạn có ý định muốn đổi sản phẩm khác hoặc không có nhu cầu mua nữa thì hãy thực hiện theo hướng dẫn sau.</p>
<p style="text-align: justify;">Tuy nhiên người dùng cần cân nhắc trước khi đặt hàng. Điều này giúp bạn suy nghĩ kỹ hơn, tránh trường hợp hủy nhiều lần hệ thống ghi nhận và ảnh hưởng trong quá trình sử dụng.</p>
